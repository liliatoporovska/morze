def code_morze(value):
    some_string = ''
    for i in value:
        if i.isalpha():
            some_string += i.upper()
    some_string = value.split()

    decoder_1 = dict({'A': '.-', 'B': '-...',
                      'C': '-.-.', 'D': '-..',
                      'E': '.', 'F': '..-.', 'G': '--.',
                      'H': '....', 'I': '..', 'J': '.---',
                      'K': '-.-', 'L': '.-..', 'M': '--',
                      'N': '-.', 'O': '---', 'P': '.--.',
                      'Q': '--.-', 'R': '.-.', 'S': '...',
                      'T': '-', 'U': '..-', 'V': '...-', 'W': '.--',
                      'X': '-..-', 'Y': '-.--', 'Z': '--..',
                      '1': '.----', '2': '..---', '3': '...--',
                      '4': '....-', '5': '.....', '6': '-....',
                      '7': '--...', '8': '---..', '9': '----.',
                      '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..',
                      '/': '-..-.', '-': '-....-',
                      '(': '-.--.', ')': '-.--.-'})
    result_string = ''
    for i in range(len(some_string)):
        for j in decoder_1.keys():
            if some_string[i] == j:
                result_string += decoder_1[j]
    return result_string

